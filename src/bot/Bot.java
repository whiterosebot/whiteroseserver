package bot;

import bot.config.ConfigurationManager;
import bot.logger.Logger;
import bot.core.notify.NotifyManager;
import bot.network.server.WebSocketServer;
import bot.users.User;
import bot.users.UsersManager;

public class Bot {


    private static Core core;
    private static ConfigurationManager configurationManager;
    private static Logger logger;

    private static WebSocketServer webSocketServer;
    private static UsersManager usersManager;
    private static NotifyManager notifyManager;
    private static TBot tbot;


    public void init() {
        Bot.configurationManager = new ConfigurationManager();
        Bot.logger = new Logger();
        Bot.webSocketServer = new WebSocketServer();
        Bot.webSocketServer.init();
        Bot.webSocketServer.connect();
        Bot.usersManager = new UsersManager();
        Bot.tbot = new TBot(Bot.configurationManager);
        Bot.notifyManager = new NotifyManager();
        if(Bot.getConfigurationManager().getBoolean("telegram"))
            Bot.tbot.start();

        Bot.core = new Core();

        try {
            Bot.core.init();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static Core getCore() {
        return core;
    }

    public static void setCore(Core core) {
        Bot.core = core;
    }

    public static ConfigurationManager getConfigurationManager() {
        return configurationManager;
    }

    public static void setConfigurationManager(ConfigurationManager configurationManager) {
        Bot.configurationManager = configurationManager;
    }

    public static TBot getTbot() {
        return tbot;
    }

    public static void setTbot(TBot tbot) {
        Bot.tbot = tbot;
    }

    public static NotifyManager getNotifyManager() {
        return notifyManager;
    }
    public static Logger getLogger() { return logger; }

    public static WebSocketServer getWebSocketServer() {
        return webSocketServer;
    }

    public static UsersManager getUsersManager() {
        return usersManager;
    }
}
