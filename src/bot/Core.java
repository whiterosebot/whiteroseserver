package bot;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Driver;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;
import java.awt.AWTException;
import java.util.Timer;

import bot.config.ConfigurationManager;
import bot.core.delays.Delays;
import bot.core.delays.DelaysColors;
import bot.core.delays.DelaysNumbers;
import bot.core.html.Parser;
import bot.core.robot.Automate;
import bot.core.roulette.Roulette;
import bot.core.sequences.Sequences;
import bot.core.sequences.SequencesColors;
import bot.core.sequences.SequencesNumbers;
import bot.utils.Emoji;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;
import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class Core {

    private Automate automate;
    private Parser parser;

    private WebDriver driver;

    private HashMap<String, Roulette> roulettes = new HashMap<String, Roulette>();

    public String NOME_ROULETTE = "Unknown Roulette";

    public Core() {
        this.automate = new Automate();
        this.prepareRoulettes();
        this.parser = new Parser();
    }

    public void init() throws InterruptedException {

        if(Bot.getConfigurationManager().getValue("driver").equals("mozilla")) {
            if (Bot.getConfigurationManager().getValue("os").equals("linux")) {
                System.setProperty("webdriver.gecko.driver", "libraries/geckodriver");
            } else {
                System.setProperty("webdriver.gecko.driver", "libraries/geckodriver.exe");
            }
            FirefoxOptions options = new FirefoxOptions();
            String userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1";
            options.addPreference("general.useragent.override", userAgent);

            this.driver = new FirefoxDriver(options);
        } else {

            if (Bot.getConfigurationManager().getValue("os").equals("linux")) {
                System.setProperty("webdriver.chrome.driver", "libraries/chromedriver");
            } else {
                System.setProperty("webdriver.chrome.driver", "libraries/chromedriver.exe");
            }

            ChromeOptions options = new ChromeOptions();
            String userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1";
            options.addArguments("--user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1");
            if(Bot.getConfigurationManager().getBoolean("headless"))
                options.addArguments("headless");
            this.driver = new ChromeDriver(options);
        }

        this.driver.manage().window().setSize(new Dimension(411, 823));
        this.driver.get("https://www.leovegas.it/it-it/roulette");


        System.out.println("[WHITEROSEBOT]\n");

        System.out.println(" *** Fai la tua scelta *** ");
        System.out.println("Seleziona un evento:");
        System.out.println(" 0 - Uscire");
        System.out.println(" 1 - Avvia l'automatizzazione");
        System.out.println(" 2 - Avvia un test");
        Scanner in = new Scanner(System.in);

        int r = in.nextInt();

        switch(r) {
            case 0:
                System.exit(1);
                break;
            case 1:
                this.start();
                in.close();
                break;

            case 2:
                System.out.println("Test");
                break;
        }
    }
    public void start() throws InterruptedException {
        System.out.println("Aspetto " + Bot.getConfigurationManager().getInt("startTime") +" secondi, dopo inizio..");
        //Thread.sleep(Bot.getConfigurationManager().getInt("startTime") * 1000);
        Bot.getNotifyManager().sendMessage(Emoji.THUMBS_UP_SIGN + "[WHITEROSE] Started");
        System.out.println("Avviato.");

        this.automate.startAutomate();
    }

    private void prepareRoulettes() {

        this.roulettes.put("Venezia Live Roulette", new Roulette("Venezia Live Roulette"));
        this.roulettes.put("Live Roulette", new Roulette("Live Roulette"));
        this.roulettes.put("Live VIP Roulette", new Roulette("Live VIP Roulette"));
        this.roulettes.put("Immersive Roulette", new Roulette("Immersive Roulette"));
        this.roulettes.put("Live Speed Roulette", new Roulette("Live Speed Roulette"));

    }

    public Automate getAutomate() {
        return automate;
    }

    public Parser getParser() {
        return parser;
    }

    public String getNOME_ROULETTE() { return NOME_ROULETTE; }

    public void setNOME_ROULETTE(String NOME_ROULETTE) { this.NOME_ROULETTE = NOME_ROULETTE; }

    public HashMap<String, Roulette> getRoulettes() {
        return roulettes;
    }

    public void setRoulettes(HashMap<String, Roulette> roulettes) {
        this.roulettes = roulettes;
    }

    public WebDriver getDriver() {
        return driver;
    }

}
