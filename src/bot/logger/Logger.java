/*
 * This document is the property of NHG Project.
 * It is considered confidential and proprietary.
 *
 * This document may not be reproduced or transmitted in any form,
 * in whole or in part, without the express written permission of
 * NHG Project. Coded by Tuttarealstep & Shux.
 */
package bot.logger;

import java.io.Writer;
import java.util.Calendar;

public class Logger {
    public final static int DEBUG = 0;
    public final static int WARNING = 1;
    public final static int ERROR = 2;
    public final static int LOADING = 3;
    public final static int LOG = 4;
    public final static int USER = 5;
    public final static int PACKET = 6;

    private int minLevel = 0;

    /**
     * Set the current level of the log manually
     *
     * @param level One of the current levels(Debug = 0, Warning = 1, Error = 2, Loading = 3...)
     */
    public void setLevel(int level) {
        this.minLevel = level;
    }

    private static Writer packetsWriter;
    private static Writer errorsWriter;
    private static Writer debugFileWriter;

    public Logger() {

    }
    public void log(String message) {
        Calendar currentTime = Calendar.getInstance();
        message = "[" + currentTime.get(Calendar.HOUR_OF_DAY) + ":" + currentTime.get(Calendar.MINUTE) + ":" + currentTime.get(Calendar.SECOND) + "] " + message;

        System.out.println(message);
    }

    /**
     * This is a complete function useful to print a message with a different type of level,
     * without set it manually
     *
     * @param type    The level (Debug = 0, Warning = 1, Error = 2, Loading = 3...)
     * @param message The message to print
     */
    public void log(int type, String message) {
        if (type < minLevel)
            return;

        switch (type) {
            case LOG:
            default:
                this.log("[LOG] " + message);
                break;
            case LOADING:
                this.log(ConsoleColor.GREEN + "[LOADING] " + ConsoleColor.RESET + message);
                break;
            case ERROR:
                this.log(ConsoleColor.RED + "[ERROR] " + ConsoleColor.RESET + message);
                break;
            case WARNING:
                this.log(ConsoleColor.YELLOW + "[WARNING] " + ConsoleColor.RESET + message);
                break;
            case DEBUG:
                this.log(ConsoleColor.CYAN + "[DEBUG] " + ConsoleColor.RESET + message);
                break;
            case USER:
                this.log(ConsoleColor.BLUE + "[USER] " + ConsoleColor.RESET + message);
                break;
            case PACKET:
                this.log(ConsoleColor.PURPLE + "[PACKET] " + ConsoleColor.RESET + message);
                break;
        }
    }

}
