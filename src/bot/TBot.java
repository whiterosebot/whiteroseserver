package bot;

import bot.config.ConfigurationManager;
import bot.utils.Emoji;
import bot.utils.StringUitls;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;

public class TBot {


    private TelegramBot tbot;
    private ConfigurationManager configurationManager;

    public TBot(ConfigurationManager configurationManager) {
        this.configurationManager = configurationManager;
    }

    public void start() {
        this.tbot = new TelegramBot(Bot.getConfigurationManager().getValue("token"));

        this.tbot.setUpdatesListener(updates -> {

            return UpdatesListener.CONFIRMED_UPDATES_ALL;
        });

        System.out.println("[Telegram bot started]");
    }

    public void notificaRitardo(int ultimoNumero, String sequenza, Emoji tipo, String nomeRoulette) {
        SendResponse response = this.tbot.execute(new SendMessage(Bot.getConfigurationManager().getValue("mainChannelID"), tipo.toString() + " " + sequenza + " - " + nomeRoulette +  "(" + ultimoNumero + ")"));
    }

    public void notificaNumeroUscito(int numero, String sequence, String rouletteName) {
        SendResponse response = this.tbot.execute(new SendMessage(Bot.getConfigurationManager().getValue("chatlogChannelID"), "[" + rouletteName + "] Ultimo numero: " + numero + " (" + StringUitls.getFirstChar(sequence) + ")"));
    }

    public void inviaMessaggio(String messaggio) {
        SendResponse response = this.tbot.execute((new SendMessage(Bot.getConfigurationManager().getValue("chatlogChannelID"), messaggio)));
    }

    public void inviaMessaggioHTML(String messaggio) {
        SendResponse response = this.tbot.execute((new SendMessage(Bot.getConfigurationManager().getValue("chatlogChannelID"), messaggio).parseMode(ParseMode.HTML)));
    }


    public void setTbot(TelegramBot tbot) {
        this.tbot = tbot;
    }

    public ConfigurationManager getConfigurationManager() {
        return configurationManager;
    }

    public void setConfigurationManager(ConfigurationManager configurationManager) {
        this.configurationManager = configurationManager;
    }
    public TelegramBot getBot() {
        return tbot;
    }
}
