package bot.messages.outgoing.numbers;

import bot.messages.outgoing.MessageComposer;
import bot.messages.outgoing.Outgoing;
import bot.messages.outgoing.ServerMessage;
import org.json.JSONObject;

public class NewNumber extends MessageComposer {

    private int number;

    public NewNumber(int number) {
        this.number = number;
    }

    @Override
    public ServerMessage compose() {
        ServerMessage serverMessage = new ServerMessage(Outgoing.NewNumber);
        serverMessage.setJsonObject(new JSONObject("{'number':" + this.number + "}"));
        return serverMessage;
    }
}
