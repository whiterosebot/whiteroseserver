package bot.messages.outgoing;

import bot.Bot;
import bot.logger.ConsoleColor;
import bot.logger.Logger;
import bot.messages.incoming.ClientMessage;
import bot.messages.incoming.Incoming;
import bot.messages.incoming.PacketMessage;
import bot.messages.incoming.handshake.LoginMessage;
import bot.network.client.Client;

import java.util.HashMap;

public class PacketManager {

    public HashMap<Integer, Class<? extends ClientMessage>> incoming;

    public PacketManager() {
        this.incoming = new HashMap<>();

        Bot.getLogger().log(Logger.LOADING, "[PACKET] Packet Manager successfully loaded.");
    }

    public void preparePackets() {
        this.registerPacket(Incoming.LOGIN_MESSAGE, LoginMessage.class);
    }

    public void registerPacket(int header, Class<? extends ClientMessage> packet) {
        if(header < 0) {
            return;
        }

        if(this.incoming.containsKey(header)) {
            Bot.getLogger().log(Logger.ERROR, "[PACKET] Packet already registered.");
        }

        this.incoming.putIfAbsent(header, packet);
    }

    public void applyIncomingPacket(Client client, PacketMessage packet) {
        if (client == null)
            return;

        try {
            if (this.incoming.containsKey(packet.getHeader())) {
                    Bot.getLogger().log(Logger.PACKET, ConsoleColor.BLUE +"[INC] "+ ConsoleColor.RESET +"[" + packet.getHeader() + "] => " + packet.getJsonObject().toString());

                final ClientMessage messageHandler = this.incoming.get(packet.getHeader()).newInstance();
                messageHandler.client = client;
                messageHandler.message = packet;
                messageHandler.handle();
            } else {
                    Bot.getLogger().log(Logger.ERROR, "[PACKET] [NOT_FOUND] [" + packet.getHeader() + "] => " + packet.getJsonObject().toString());
            }
        } catch (Exception e) {
                e.printStackTrace();

            Bot.getLogger().log(Logger.ERROR, "[PACKET] in packet handling!");
        }
    }

}
