package bot.messages.outgoing.lights;

import bot.messages.outgoing.MessageComposer;
import bot.messages.outgoing.Outgoing;
import bot.messages.outgoing.ServerMessage;
import org.json.JSONObject;

public class NoticeLight extends MessageComposer {

    private int decision;
    private String color;
    private String roulette;

    public NoticeLight(int number, String color, String roulette) {
        this.decision = number;
        this.color = color;
        this.roulette = roulette;
    }

    @Override
    public ServerMessage compose() {
        ServerMessage serverMessage = new ServerMessage(Outgoing.NoticeLight);
        serverMessage.setJsonObject(new JSONObject("{'decision':" + this.decision + ", 'color': " + this.color + ", 'roulette': " + this.roulette + "}"));
        return serverMessage;
    }
}
