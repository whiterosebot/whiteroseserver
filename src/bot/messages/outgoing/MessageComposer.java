package bot.messages.outgoing;

public abstract class MessageComposer {
    public abstract ServerMessage compose();
}
