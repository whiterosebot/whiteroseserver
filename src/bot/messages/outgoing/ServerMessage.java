package bot.messages.outgoing;

import org.json.JSONObject;

public class ServerMessage {
    private Integer header;
    private JSONObject jsonObject;

    public ServerMessage(Integer header) {
        this.header = header;
        this.jsonObject = new JSONObject("{}");
    }

    public ServerMessage init(Integer header) {
        this.header = header;
        return this;
    }

    public Integer getHeader() {
        return header;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public JSONObject getJsonObject() {
        return this.jsonObject;
    }

    public void append(String key, Object object) {
        this.jsonObject.append(key, object);
    }

    public void put(String key, Object object) {
        this.jsonObject.put(key, object);
    }

    public String getResponse() {
        return "{\"header\": " + this.getHeader() + ", \"body\": " + this.jsonObject.toString() + "}";
    }
}
