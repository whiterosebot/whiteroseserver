package bot.messages.incoming;

import org.json.JSONArray;
import org.json.JSONObject;


public class PacketMessage {
    private final Integer header;
    private final JSONObject jsonObject;

    public PacketMessage(Integer header, JSONObject jsonObject) {
        this.header = header;
        this.jsonObject = jsonObject;
    }

    public Integer getHeader() {
        return this.header;
    }

    public JSONObject getJsonObject() {
        return this.jsonObject;
    }

    public Boolean getBoolean(String key) {
        if (key == null) return null;

        return this.jsonObject.getBoolean(key);
    }

    public String getString(String key) {
        if (key == null) return null;

        return this.jsonObject.getString(key);
    }

    public Integer getInteger(String key) {
        if (key == null) return null;

        return this.jsonObject.getInt(key);
    }

    public Double getDouble(String key) {
        if (key == null) return null;

        return this.jsonObject.getDouble(key);
    }

    public Long getLong(String key) {
        if (key == null) return null;

        return this.jsonObject.getLong(key);
    }

    public JSONArray getJSONArray(String key) {
        if (key == null) return null;

        return this.jsonObject.getJSONArray(key);
    }

    public JSONObject getJSONObject(String key) {
        if (key == null) return null;

        return this.jsonObject.getJSONObject(key);
    }
}

