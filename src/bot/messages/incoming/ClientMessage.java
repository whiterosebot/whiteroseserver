package bot.messages.incoming;

import bot.network.client.Client;

public abstract class ClientMessage {
    public Client client;
    public PacketMessage message;

    public abstract void handle() throws Exception;
}
