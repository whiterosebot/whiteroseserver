package bot.messages.incoming.handshake;

import bot.Bot;
import bot.messages.incoming.ClientMessage;
import bot.users.User;

public class LoginMessage extends ClientMessage {
    @Override
    public void handle() throws Exception {
        User user = new User("UserAAA");
        user.setClient(this.client);
        Bot.getUsersManager().addUser(user);
        this.client.setUser(user);
    }
}
