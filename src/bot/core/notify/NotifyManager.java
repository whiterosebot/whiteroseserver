package bot.core.notify;

import bot.Bot;
import bot.utils.Emoji;

public class NotifyManager {

    public NotifyManager() {

    }

    public void notifyDelay(int ultimoNumero, String sequenza, Emoji tipo, String nomeRoulette){
        if(Bot.getConfigurationManager().getBoolean("telegram")) {
            Bot.getTbot().notificaRitardo(ultimoNumero, sequenza, tipo, nomeRoulette);
        } else {
            System.out.println(sequenza + " - " + nomeRoulette +  "(" + ultimoNumero + ")");
        }
    }

    public void sendMessage(String messaggio) {
        if(Bot.getConfigurationManager().getBoolean("telegram")) {
            Bot.getTbot().inviaMessaggio(messaggio);
        } else {
            System.out.println(messaggio);
        }
    }

    public void sendHtmlMessage(String messaggio) {
        if(Bot.getConfigurationManager().getBoolean("telegram")) {
            Bot.getTbot().inviaMessaggioHTML(messaggio);
        } else {
            System.out.println(messaggio);
        }
    }

    public void notifyNumber(int ultimoNumero, String sequenza, String nomeRoulette) {
        if(Bot.getConfigurationManager().getBoolean("telegram")) {
            Bot.getTbot().notificaNumeroUscito(ultimoNumero, sequenza, nomeRoulette);
        } else {
            System.out.println(sequenza + " - " + nomeRoulette +  "(" + ultimoNumero + ")");
        }
    }

}
