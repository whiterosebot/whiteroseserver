package bot.core.roulette;

import bot.Bot;
import bot.core.delays.Delays;
import bot.core.delays.DelaysColors;
import bot.core.delays.DelaysNumbers;
import bot.core.sequences.Sequences;
import bot.core.sequences.SequencesColors;
import bot.core.sequences.SequencesNumbers;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Roulette {


    private String rouletteName;

    private DelaysColors delays;
    private SequencesColors sequences;

    public Roulette(String name) {
        this.rouletteName = name;
        this.delays = new DelaysColors();
        this.sequences = new SequencesColors();
    }

    public void handle(ArrayList sequence) {
        this.sequences.processSequence(this, sequence, this.rouletteName);
    }

    public String getLinkFromRouletteName(String rouletteName) {

        String link = rouletteName.toLowerCase().replace(" ", "-");

        switch(rouletteName) {
            default:
                return link;
            case "Immersive Roulette":
                return link + "-";
            case "Live Speed Roulette":
                return link + "-";
        }

    }

    public void refresh() {
        this.delays.refresh();
    }

    public DelaysColors getDelays() {
        return delays;
    }

    public SequencesColors getSequences() {
        return sequences;
    }

    public String getRouletteName() {
        return rouletteName;
    }

    public void setRouletteName(String rouletteName) {
        this.rouletteName = rouletteName;
    }

}
