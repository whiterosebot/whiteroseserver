package bot.core.html;

import bot.Bot;
import bot.core.roulette.Roulette;
import bot.utils.Emoji;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Parser {

    private boolean hasSetName = false;

    private HashMap<String, String> collectedSequences = new HashMap<String, String>();

    public Parser() {

    }

    public void parseHtmlFromFile() {
        Document doc = Jsoup.parse(Bot.getCore().getDriver().getPageSource());
        Elements containers = doc.select("div.Wxj0K");

        if(containers.size() > 1)
        {

            for(int i = 0; i < containers.size(); i++) {

                ArrayList<String> sequenza = new ArrayList<String>(5);
                Element elementoVenezia = containers.get(i);
                Element sequenzaVenezia = elementoVenezia.select("div._1Sr24").first();
                Element name = elementoVenezia.select("div._2ae78").first();
                Element lastNumber = elementoVenezia.select("div._1VKWw").first();
                Elements otherNumbers = elementoVenezia.select("div._2nl7f > span[class]");

                if(lastNumber != null) {
                    sequenza.add(lastNumber.text());

                    for(Element number : otherNumbers) {
                        sequenza.add(number.text());
                    }

                    sequenza.add(lastNumber.text());

                    if(!lastNumber.text().equals("")) {

                        //System.out.println("S: " + name.text() + " | " + sequenza.toString());

                        switch(name.text()) {

                            case "Venezia Live Roulette":
                                Bot.getCore().getRoulettes().get("Venezia Live Roulette").handle(sequenza);
                                //Bot.getNotifyManager().sendMessage("Currently running...");
                                break;
                            case "Live Roulette":
                                Bot.getCore().getRoulettes().get("Live Roulette").handle(sequenza);
                                break;
                            case "Live VIP Roulette":
                                Bot.getCore().getRoulettes().get("Live VIP Roulette").handle(sequenza);
                                break;
                            case "Immersive Roulette":
                                Bot.getCore().getRoulettes().get("Immersive Roulette").handle(sequenza);
                                break;
                            case "Live Speed Roulette":
                                Bot.getCore().getRoulettes().get("Live Speed Roulette").handle(sequenza);
                                break;
                        }
                    }
                }
            }
        }
        else {
            Bot.getNotifyManager().sendMessage("Impossibile trovare le roulette..");
        }
    }

}
