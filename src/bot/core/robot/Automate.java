package bot.core.robot;

import bot.Bot;

import java.awt.*;
import java.awt.event.KeyEvent;

import com.sun.jna.Native;

public class Automate {

    private Robot robot;

    private boolean isPageReloading = false;

    public Automate() {
        try {
            this.robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public void startAutomate() throws InterruptedException {
        while(!this.isPageReloading) {
            try {
                this.automatePickingNumbers();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Thread.sleep(Bot.getConfigurationManager().getInt("pauseTime") * 1000);
        }
    }

    private void automatePickingNumbers() throws InterruptedException {

        Thread.sleep(2000);
        Bot.getCore().getParser().parseHtmlFromFile();

    }

    public void automateAfk() throws InterruptedException {

        this.robot.keyPress(KeyEvent.VK_ALT);
        Thread.sleep(30);
        this.robot.keyPress(KeyEvent.VK_TAB);
        Thread.sleep(30);
        this.robot.keyRelease(KeyEvent.VK_TAB);
        Thread.sleep(30);
        this.robot.keyRelease(KeyEvent.VK_ALT);
        Thread.sleep(30);

        this.isPageReloading = true;

        Thread.sleep(4000);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.delay(100);
        robot.keyPress(KeyEvent.VK_SHIFT);
        robot.delay(100);
        robot.keyPress(KeyEvent.VK_R);

        robot.delay(100);
        robot.keyRelease(KeyEvent.VK_R);
        robot.delay(100);
        robot.keyRelease(KeyEvent.VK_SHIFT);
        robot.delay(100);
        robot.keyRelease(KeyEvent.VK_CONTROL);

        Thread.sleep(13000);

        robot.mouseMove(Bot.getConfigurationManager().getInt("mouseButtonEnterX"), Bot.getConfigurationManager().getInt("mouseButtonEnterY"));

        Thread.sleep(500);

        robot.mousePress(KeyEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(KeyEvent.BUTTON1_DOWN_MASK);

        Thread.sleep(3000);

        this.robot.keyPress(KeyEvent.VK_CONTROL);
        this.robot.keyPress(KeyEvent.VK_SHIFT);
        this.robot.keyPress(KeyEvent.VK_C);
        Thread.sleep(200);
        this.robot.keyRelease(KeyEvent.VK_CONTROL);
        this.robot.keyRelease(KeyEvent.VK_SHIFT);
        this.robot.keyRelease(KeyEvent.VK_C);

        Thread.sleep(1000);

        this.robot.mouseMove(Bot.getConfigurationManager().getInt("mouseInspectX"), Bot.getConfigurationManager().getInt("mouseInspectY"));

        Thread.sleep(1200);

        this.robot.mousePress(KeyEvent.BUTTON1_DOWN_MASK);
        this.robot.mouseRelease(KeyEvent.BUTTON1_DOWN_MASK);

        for(int i = 0; i <= Bot.getConfigurationManager().getInt("timesKeyUp"); i++) {
            robot.keyPress(KeyEvent.VK_UP);
            robot.keyRelease(KeyEvent.VK_UP);
            Thread.sleep(300);
        }

        Thread.sleep(1000);

        this.isPageReloading = false;
        this.startAutomate();
    }

    public void getMousePosition() {
        try {
            Thread.sleep(4000);
            System.out.println("Mouse Position: " + MouseInfo.getPointerInfo().getLocation().toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void testInput() throws InterruptedException {
        System.out.println("Sto testando il mouse...");
        Thread.sleep(4000);
        this.robot.mouseMove(400, 400);
    }

    public boolean isPageReloading() {
        return isPageReloading;
    }

    public void setPageReloading(boolean pageReloading) {
        isPageReloading = pageReloading;
    }

}
