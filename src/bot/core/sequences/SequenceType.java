package bot.core.sequences;

public enum SequenceType {
    BIONDA,
    MORA,
    VUOTA,
    ROSSO,
    NERO,
    VERDE
}
