package bot.core.sequences;

import bot.Bot;
import bot.core.roulette.Roulette;
import bot.messages.outgoing.numbers.NewNumber;
import bot.users.User;

import javax.sound.midi.Sequence;
import java.util.ArrayList;

public class SequencesColors {

    private static final Integer[] NERI = new Integer[]{2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35};
    private static final Integer[] ROSSI = new Integer[]{1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36};
    private static final Integer[] VERDI = new Integer[]{0}; // vuoto

    protected ArrayList<String> lastSequence = new ArrayList<String>();

    public void processSequence(Roulette roulette, ArrayList<String> sequence, String rouletteName) {

        if(this.lastSequence.size() != 0) {
            if(!(sequence.equals(this.lastSequence))) {
                String number = sequence.get(0);
                if(!number.equals(" ")) {
                    int num = Integer.parseInt(number);
                    System.out.println("L'ultimo numero uscito nella " + roulette.getRouletteName() + " è il: " + num);
                    SequenceType type = this.getSequenceTypeFromNumber(num);
                    String sequenceName = this.getSequenceNameFromType(type);
                    if (roulette.getRouletteName().equals("Venezia Live Roulette")) {
                        //Bot.getNotifyManager().sendHtmlMessage("<a href="+roulette.getLinkFromRouletteName(rouletteName)+">Gioca</a>");
                        Bot.getNotifyManager().notifyNumber(num, sequenceName, rouletteName);

                        for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                            user.getClient().sendResponse(new NewNumber(num).compose());
                        }
                    }
                    try {
                        roulette.getDelays().processDelay(roulette, type, sequenceName, num);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            /*for(int k = 0; k < sequence.size(); k++) {
                int num = Integer.parseInt(sequence.get(k));

                SequenceType type = this.getSequenceTypeFromNumber(num);
                String sequenceName = this.getSequenceNameFromType(type);

                try {
                    roulette.getDelays().processDelay(roulette, type, sequenceName, num);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //Bot.getNotifyManager().sendMessage("Prima sequenza..." + sequence.toString());*/
        }

        this.lastSequence = sequence;
    }


    protected String getSequenceNameFromType(SequenceType type) {
        switch(type) {
            case NERO:
                return "NERO";
            case ROSSO:
                return "ROSSO";
            case VERDE:
                return "VUOTO";
        }

        return "";
    }

    public boolean isContained(int numero, SequenceType sequenceType) {
        switch(sequenceType) {
            case NERO:
                for(int i = 0; i < SequencesColors.NERI.length; i++) {
                    if (numero == SequencesColors.NERI[i]) {
                        i = SequencesColors.NERI.length;
                        return true;
                    }
                }
                break;
            case ROSSO:
                for(int j = 0; j < SequencesColors.ROSSI.length; j++) {
                    if(SequencesColors.ROSSI[j] == numero) {
                        j = SequencesColors.ROSSI.length;
                        return true;
                    }
                }
                break;
            case VERDE:
                for(int k = 0; k < SequencesColors.VERDI.length; k++) {
                    if(SequencesColors.VERDI[k] == numero) {
                        k = SequencesColors.VERDI.length;
                        return true;
                    }
                }
                break;
        }

        return false;
    }

    protected SequenceType getSequenceTypeFromNumber(int number) {
        SequenceType returnedSequence = SequenceType.MORA;

        for(int i = 0; i < SequencesColors.NERI.length; i++) {
            if(number == SequencesColors.NERI[i]) {
                i = SequencesColors.NERI.length;
                returnedSequence = SequenceType.NERO;
            }
            else {
                for(int j = 0; j < SequencesColors.ROSSI.length; j++) {
                    if(SequencesColors.ROSSI[j] == number) {
                        j = SequencesColors.ROSSI.length;
                        returnedSequence = SequenceType.ROSSO;
                    }
                }
                for(int k = 0; k < SequencesColors.VERDI.length; k++) {
                    if(SequencesColors.VERDI[k] == number) {
                        k = SequencesColors.VERDI.length;
                        returnedSequence = SequenceType.VERDE;
                    }
                }
            }
        }
        return returnedSequence;
    }

    public ArrayList<String> getLastSequence() {
        return lastSequence;
    }

    public void setLastSequence(ArrayList<String> lastSequence) {
        this.lastSequence = lastSequence;
    }
}
