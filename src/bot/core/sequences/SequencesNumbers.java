package bot.core.sequences;

import bot.Bot;

import javax.sound.midi.Sequence;

public class SequencesNumbers extends Sequences {

    private static final Integer[] MORA = new Integer[]{2, 3, 4, 5, 9, 11, 14, 17, 19, 24, 25, 27, 29, 30, 33, 35};
    private static final Integer[] BIONDA = new Integer[]{0, 1, 6, 7, 8, 10, 15, 16, 20, 21, 22, 23, 26, 28, 32, 34};
    private static final Integer[] VUOTA = new Integer[]{ 12, 13, 18, 31, 36}; // sono i numeri che non andremo a giocare

    public SequencesNumbers() {

    }

    public SequenceType getSequenceType() {
        return SequenceType.MORA;
    }

    public String getSequenceNameFromType(SequenceType type) {
        switch(type) {
            case MORA:
                return "MORA";
            case BIONDA:
                return "BIONDA";
            case VUOTA:
                return "VUOTA";
        }

        return "";
    }


    public boolean isContained(int numero, SequenceType sequenceType) {
        switch(sequenceType) {
            case MORA:
                for(int j = 0; j < SequencesNumbers.MORA.length; j++) {
                    if(SequencesNumbers.MORA[j] == numero) {
                        j = SequencesNumbers.MORA.length;
                        return true;
                    }
                }
                break;
            case BIONDA:
                for(int j = 0; j < SequencesNumbers.BIONDA.length; j++) {
                    if(SequencesNumbers.BIONDA[j] == numero) {
                        j = SequencesNumbers.BIONDA.length;
                        return true;
                    }
                }
                break;
            case VUOTA:
                for(int j = 0; j < SequencesNumbers.VUOTA.length; j++) {
                    if(SequencesNumbers.VUOTA[j] == numero) {
                        j = SequencesNumbers.VUOTA.length;
                        return true;
                    }
                }
                break;
        }
        return false;
    }
    public SequenceType getSequenceTypeFromNumber(int number) {
        SequenceType returnedSequence = SequenceType.MORA;

        for(int i = 0; i < SequencesNumbers.VUOTA.length; i++) {
            if(number == SequencesNumbers.VUOTA[i]) {
                i = SequencesNumbers.VUOTA.length;
                returnedSequence = SequenceType.VUOTA;
            }
            else {
                for(int j = 0; j < SequencesNumbers.BIONDA.length; j++) {
                    if(SequencesNumbers.BIONDA[j] == number) {
                        j = SequencesNumbers.BIONDA.length;
                        returnedSequence = SequenceType.BIONDA;
                    }
                }
                for(int k = 0; k < SequencesNumbers.MORA.length; k++) {
                    if(SequencesNumbers.MORA[k] == number) {
                        k = SequencesNumbers.MORA.length;
                        returnedSequence = SequenceType.MORA;
                    }
                }
            }
        }
        return returnedSequence;
    }

}
