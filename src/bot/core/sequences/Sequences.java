package bot.core.sequences;

import bot.Bot;
import bot.core.roulette.Roulette;

import java.util.ArrayList;

public abstract class Sequences {

    protected  boolean equalLists(ArrayList one, ArrayList two){
        if (one == null && two == null){
            return true;
        }

        if((one == null && two != null)
                || one != null && two == null
                || one.size() != two.size()){
            return false;
        }

        return one.equals(two);
    }

    public abstract boolean isContained(int numero, SequenceType sequenceType);
    protected abstract String getSequenceNameFromType(SequenceType type);
    protected abstract SequenceType getSequenceTypeFromNumber(int numero);


}
