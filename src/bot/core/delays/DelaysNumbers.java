package bot.core.delays;

import bot.Bot;
import bot.core.roulette.Roulette;
import bot.core.sequences.SequenceType;
import bot.utils.Emoji;

import javax.sound.midi.Sequence;

public class DelaysNumbers extends Delays {

    private int totaleRitardiMora = 0;
    private int totaleRitardiBionda = 0;

    public DelaysNumbers() {

    }

    public void processDelay(Roulette roulette, SequenceType sequenceType, String sequenceName, int num) throws InterruptedException {


    }

    public void createDelays(SequenceType type) {
        switch(type) {
            case MORA:
                this.totaleRitardiBionda++;
                this.totaleRitardiMora = 0;
                break;
            case BIONDA:
                this.totaleRitardiMora++;
                this.totaleRitardiBionda = 0;
                break;
            case VUOTA:
                this.totaleRitardiMora++;
                this.totaleRitardiBionda++;
                break;
        }
    }

    public int getRitardiMora() {
        return totaleRitardiMora;
    }

    public void setRitardiMora(int ritardiMora) {
        this.totaleRitardiMora = ritardiMora;
    }

    public int getRitardiBionda() {
        return totaleRitardiBionda;
    }

    public void setRitardiBionda(int ritardiBionda) {
        this.totaleRitardiBionda = ritardiBionda;
    }

}
