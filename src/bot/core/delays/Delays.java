package bot.core.delays;

import bot.core.roulette.Roulette;
import bot.core.sequences.SequenceType;

public abstract class Delays {

    public abstract void processDelay(Roulette roulette, SequenceType sequenceType, String sequenceName, int num) throws InterruptedException;
    public abstract void createDelays(SequenceType type);

}
