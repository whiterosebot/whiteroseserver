package bot.core.delays;

import bot.Bot;
import bot.core.roulette.Roulette;
import bot.core.sequences.SequenceType;
import bot.messages.outgoing.lights.NoticeLight;
import bot.messages.outgoing.numbers.NewNumber;
import bot.users.User;
import bot.utils.Emoji;

public class DelaysColors {

    private int totaleRitardiRosso;
    private int totaleRitardiNero;

    private int giroDopoSemaforoNeri = 0;
    private int giroDopoSemaforoRossi = 0;

    public DelaysColors() {
        this.totaleRitardiRosso = 0;
        this.totaleRitardiNero = 0;
    }

    public void refresh() {
        this.totaleRitardiRosso = 0;
        this.totaleRitardiNero = 0;
        this.giroDopoSemaforoNeri = 0;
        this.giroDopoSemaforoRossi = 0;
    }

    public void processDelay(Roulette roulette, SequenceType sequenceType, String sequenceName, int num) throws InterruptedException {

        this.createDelays(sequenceType);

        if(this.totaleRitardiNero == Bot.getConfigurationManager().getInt("delaysColors")-2) {
            Bot.getNotifyManager().notifyDelay(num, "N - Preparati", Emoji.RED_CIRCLE, roulette.getRouletteName());
            for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                user.getClient().sendResponse(new NoticeLight(0, "N", roulette.getRouletteName()).compose());
            }
            this.giroDopoSemaforoNeri = Bot.getConfigurationManager().getInt("delaysColors")-2;
            this.giroDopoSemaforoNeri++;
        }
        else if(this.giroDopoSemaforoNeri == Bot.getConfigurationManager().getInt("delaysColors")-1) {
            if(roulette.getSequences().isContained(num, SequenceType.NERO)) {
                Bot.getNotifyManager().notifyDelay(num, "N - OUT", Emoji.CROSS_MARK, roulette.getRouletteName());
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(3, "N", roulette.getRouletteName()).compose()); // out
                }
                this.giroDopoSemaforoNeri = 0;
                this.totaleRitardiNero = 0;
            }
            else {
                Bot.getNotifyManager().notifyDelay(num, "N - Preparati", Emoji.ORANGE_CIRCLE, roulette.getRouletteName());
                this.giroDopoSemaforoNeri = Bot.getConfigurationManager().getInt("delaysColors")-1;
                this.giroDopoSemaforoNeri++;
            }
        }
        else if(this.giroDopoSemaforoNeri == Bot.getConfigurationManager().getInt("delaysColors")) {
            if(roulette.getSequences().isContained(num, SequenceType.NERO)) {
                Bot.getNotifyManager().notifyDelay(num, "N - OUT", Emoji.CROSS_MARK, roulette.getRouletteName());
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(3, "N", roulette.getRouletteName()).compose()); // out
                }
                this.giroDopoSemaforoNeri = 0;
                this.totaleRitardiNero = 0;
            }
            else {
                Bot.getNotifyManager().notifyDelay(num, "N - Gioca", Emoji.GREEN_CIRCLE, roulette.getRouletteName());
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(2, "N", roulette.getRouletteName()).compose());
                }
                this.totaleRitardiNero = 0;
                this.totaleRitardiRosso = 0;
                this.giroDopoSemaforoNeri = Bot.getConfigurationManager().getInt("delaysColors");
                this.giroDopoSemaforoNeri++;
                Bot.getNotifyManager().sendMessage(Emoji.NEGATIVE_SQUARED_CROSS_MARK + " Reset condition on " + roulette.getRouletteName());
            }
        }

        else if(this.giroDopoSemaforoNeri == Bot.getConfigurationManager().getInt("delaysColors")+1) {
            if(roulette.getSequences().isContained(num, SequenceType.ROSSO)) {
                Bot.getNotifyManager().sendMessage("[" + roulette.getRouletteName() + "] N - Raddoppia!");
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(4, "N", roulette.getRouletteName()).compose());
                }
            }
            else {
                Bot.getNotifyManager().sendMessage("[" + roulette.getRouletteName() + "] N - Vinta");
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(3, "N", roulette.getRouletteName()).compose()); // fermati
                }
                this.giroDopoSemaforoNeri = 0;

            }
        }
        else if(this.giroDopoSemaforoNeri == Bot.getConfigurationManager().getInt("delaysColors")+2) {
            if(roulette.getSequences().isContained(num, SequenceType.ROSSO)) {
                Bot.getNotifyManager().sendMessage("[" + roulette.getRouletteName() + "] N - Persa :(");
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(3, "N", roulette.getRouletteName()).compose());
                }
                this.giroDopoSemaforoNeri = 0;
            }
            else {
                Bot.getNotifyManager().sendMessage("[" + roulette.getRouletteName() + "] N - Vinta");
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(3, "N", roulette.getRouletteName()).compose()); // fermati
                }
                this.giroDopoSemaforoNeri = 0;
            }
        }


        // rossi
        if(this.totaleRitardiRosso == Bot.getConfigurationManager().getInt("delaysColors")-2) {
            Bot.getNotifyManager().notifyDelay(num, "R - Preparati", Emoji.RED_CIRCLE, roulette.getRouletteName());
            for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                user.getClient().sendResponse(new NoticeLight(0, "R", roulette.getRouletteName()).compose());
            }
            this.giroDopoSemaforoRossi = Bot.getConfigurationManager().getInt("delaysColors")-2;
            this.giroDopoSemaforoRossi++;
        }
        else if(this.giroDopoSemaforoRossi == Bot.getConfigurationManager().getInt("delaysColors")-1) {
            if(roulette.getSequences().isContained(num, SequenceType.ROSSO)) {
                Bot.getNotifyManager().notifyDelay(num, "R - OUT", Emoji.CROSS_MARK, roulette.getRouletteName());
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(3, "R", roulette.getRouletteName()).compose()); // out
                }
                this.giroDopoSemaforoRossi = 0;
                this.totaleRitardiRosso = 0;
            }
            else {
                Bot.getNotifyManager().notifyDelay(num, "R - Preparati", Emoji.ORANGE_CIRCLE, roulette.getRouletteName());
                this.giroDopoSemaforoRossi = Bot.getConfigurationManager().getInt("delaysColors")-1;
                this.giroDopoSemaforoRossi++;
            }
        }
        else if(this.giroDopoSemaforoRossi == Bot.getConfigurationManager().getInt("delaysColors")) {
            if(roulette.getSequences().isContained(num, SequenceType.ROSSO)) {
                Bot.getNotifyManager().notifyDelay(num, "R - OUT", Emoji.CROSS_MARK, roulette.getRouletteName());
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(3, "R", roulette.getRouletteName()).compose()); // out
                }
                this.giroDopoSemaforoRossi = 0;
                this.totaleRitardiRosso = 0;
            }
            else {
                Bot.getNotifyManager().notifyDelay(num, "R - Gioca", Emoji.GREEN_CIRCLE, roulette.getRouletteName());
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(2, "N", roulette.getRouletteName()).compose());
                }
                this.totaleRitardiNero = 0;
                this.totaleRitardiRosso = 0;
                this.giroDopoSemaforoRossi = 0;
                Bot.getNotifyManager().sendMessage(Emoji.NEGATIVE_SQUARED_CROSS_MARK + " Reset condition on " + roulette.getRouletteName());
            }
        }
        else if(this.giroDopoSemaforoRossi == Bot.getConfigurationManager().getInt("delaysColors")+1) {
            if(roulette.getSequences().isContained(num, SequenceType.NERO)) {
                Bot.getNotifyManager().sendMessage("[" + roulette.getRouletteName() + "] R - Raddoppia!");
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(4, "R", roulette.getRouletteName()).compose());
                }
            }
            else {
                Bot.getNotifyManager().sendMessage("[" + roulette.getRouletteName() + "] R - Vinta");
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(3, "R", roulette.getRouletteName()).compose()); // fermati
                }
                this.giroDopoSemaforoRossi = 0;
            }
        }
        else if(this.giroDopoSemaforoRossi == Bot.getConfigurationManager().getInt("delaysColors")+2) {
            if(roulette.getSequences().isContained(num, SequenceType.NERO)) {
                Bot.getNotifyManager().sendMessage("[" + roulette.getRouletteName() + "] R - Persa :(");
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(3, "R", roulette.getRouletteName()).compose());
                }
                this.giroDopoSemaforoRossi = 0;
            }
            else {
                Bot.getNotifyManager().sendMessage("[" + roulette.getRouletteName() + "] R - Vinta");
                for(User user : Bot.getUsersManager().getActiveUsers().values()) {
                    user.getClient().sendResponse(new NoticeLight(3, "R", roulette.getRouletteName()).compose()); // fermati
                }
                this.giroDopoSemaforoRossi = 0;
            }
        }
    }

    public void createDelays(SequenceType type) {
        switch(type) {
            case NERO:
                this.totaleRitardiRosso++;
                this.totaleRitardiNero = 0;
                break;
            case ROSSO:
                this.totaleRitardiNero++;
                this.totaleRitardiRosso = 0;
                break;
            case VERDE:
                this.totaleRitardiRosso++;
                this.totaleRitardiNero++;
                break;
        }
    }
}
