package bot.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigurationManager {

    private final Properties properties;
    private boolean isLoading = false;

    public ConfigurationManager() {
        this.properties = new Properties();
        this.isLoading = true;
        this.load();
    }

    public void load() {

        InputStream input = null;

        try {
            File f = new File("config.ini");
            input = new FileInputStream(f);
            this.properties.load(input);

        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        this.isLoading = false;
    }

    public int getInt(String key) {
        return Integer.parseInt(this.getValue(key));
    }

    public String getValue(String key) {
        return this.properties.get(key).toString();
    }

    public double getDouble(String key) {
        return Double.parseDouble(this.properties.get(key).toString());
    }

    public boolean getBoolean(String key) {
        return this.getInt(key) == 1;
    }

}
