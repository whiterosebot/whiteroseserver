package bot.users;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UsersManager {

    private final ConcurrentHashMap<String, User> activeUsers; // username - instance

    public UsersManager() {
        this.activeUsers = new ConcurrentHashMap<>();
    }

    public void addUser(User user) {
        this.activeUsers.put(user.getUsername(), user);
    }

    public void removeUser(User user) {
        this.activeUsers.remove(user.getUsername());
    }

    public User getUser(int id) {
        return this.activeUsers.get(id);
    }

    public User getUser(String name) {
        synchronized (this.activeUsers) {
            for (Map.Entry<String, User> map : this.activeUsers.entrySet()) {
                if (map.getValue().getUsername().equalsIgnoreCase(name))
                    return map.getValue();
            }
        }

        return null;
    }


    public ConcurrentHashMap<String, User> getActiveUsers() {
        return activeUsers;
    }
}
