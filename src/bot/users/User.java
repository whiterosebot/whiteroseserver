package bot.users;

import bot.network.client.Client;

public class User {

    private String username;
    private Client client;

    public User(String user) {
        this.username = user;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Client getClient() {
        return client;
    }

    public String getUsername() {
        return username;
    }
}
