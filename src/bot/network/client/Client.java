package bot.network.client;

import bot.Bot;
import bot.logger.ConsoleColor;
import bot.logger.Logger;
import bot.messages.outgoing.ServerMessage;
import bot.users.User;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

public class Client {

    private Channel channel;
    private User user;

    public Client(Channel channel) {
        this.channel = channel;
    }

    public void disconnect() {
        this.channel.close();
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void sendResponse(ServerMessage serverMessage) {
        if (this.channel.isOpen()) {
            if (serverMessage == null || serverMessage.getHeader() <= 0)
                return;

            Bot.getLogger().log(Logger.PACKET, ConsoleColor.GREEN +"[OUT] "+ ConsoleColor.RESET +"[" + serverMessage.getHeader() + "] => " + serverMessage.getResponse());

            this.channel.writeAndFlush(this.channel.writeAndFlush(new TextWebSocketFrame(serverMessage.getResponse())));
        }
    }


}
