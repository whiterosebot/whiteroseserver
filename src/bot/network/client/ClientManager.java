package bot.network.client;

import bot.users.User;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import io.netty.util.AttributeKey;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ClientManager {

    private final ConcurrentMap<ChannelId, Client> clients;

    public static final AttributeKey<Client> CLIENT = AttributeKey.valueOf("Client");

    public ClientManager() {
        this.clients = new ConcurrentHashMap<>();
    }

    public ConcurrentMap<ChannelId, Client> getSessions() {
        return this.clients;
    }

    public boolean containsClient(Channel channel) {
        return this.clients.containsKey(channel.id());
    }

    public Client getClient(Channel channel) {
        if (this.clients.containsKey(channel.id()))
            return this.clients.get(channel.id());

        return null;
    }

    public Client getClient(User user) {
        for (Client client : this.clients.values()) {
            if (client.getUser() == user)
                return client;
        }

        return null;
    }


    public boolean addClient(ChannelHandlerContext channelHandlerContext) {
        Client client = new Client(channelHandlerContext.channel());
        channelHandlerContext.channel().closeFuture().addListener(channelFuture -> disconnectClient(channelHandlerContext.channel()));

        channelHandlerContext.attr(CLIENT).set(client);
        //channelHandlerContext.channel().attr(CLIENT).set(client);
        channelHandlerContext.fireChannelRegistered();

        return this.clients.putIfAbsent(channelHandlerContext.channel().id(), client) == null;
    }

    public void disconnectClient(Channel channel) {
        Client client = this.getClient(channel);

        if (client != null)
            client.disconnect();

        channel.attr(CLIENT).set(null);
        channel.deregister();
        channel.close();
        this.clients.remove(channel.id());
    }

    public Boolean containsUser(int id) {
        return false;
    }


}
