package bot.network.server;

import bot.Bot;
import bot.logger.Logger;
import bot.messages.outgoing.PacketManager;
import bot.network.client.Client;
import bot.network.client.ClientManager;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;

public class WebSocketServer {

    private final int port = 8080;

    private final String host = "0.0.0.0";

    private final ServerBootstrap serverBootstrap;
    private final EventLoopGroup bossGroup;
    private final EventLoopGroup workerGroup;

    private final ClientManager clientManager;

    private PacketManager packetManager;



    public WebSocketServer() {
        this.serverBootstrap = new ServerBootstrap();
        this.bossGroup = new NioEventLoopGroup(1);
        this.workerGroup = new NioEventLoopGroup(1);
        this.clientManager = new ClientManager();
        this.packetManager = new PacketManager();
        this.packetManager.preparePackets();

    }

    public void init() {
        this.serverBootstrap.group(this.bossGroup, this.workerGroup);
        this.serverBootstrap.channel(NioServerSocketChannel.class);
        this.serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {

            @Override
            public void initChannel(SocketChannel ch) throws Exception {
                ChannelPipeline pipeline = ch.pipeline();
                pipeline.addLast("httpServerCodec", new HttpServerCodec());
                pipeline.addLast("httpServerHandler", new HttpServerHandler());
                pipeline.addLast("idleStateHandler", new IdleStateHandler(0, 0, 60) {
                    @Override
                    protected void channelIdle(ChannelHandlerContext channelHandlerContext, IdleStateEvent event) throws Exception {
                        channelHandlerContext.close();
                    }
                });
            }
        });

    }

    public void connect() {
        try {
            Channel channel = this.serverBootstrap.bind(this.host, this.port).sync().channel();
            Bot.getLogger().log(Logger.LOADING, "[WEBSOCKET] Started WebSocket server on " + this.host + ":" + this.port);
        } catch (Exception e) {
                e.printStackTrace();

            Bot.getLogger().log(Logger.ERROR, "[WEBSOCKET] Failed to connect to the host (" + this.host + ":" + this.port + ").");
            System.exit(0);
        }

    }

    public void stop() {

    }

    public PacketManager getPacketManager() {
        return packetManager;
    }

    public ClientManager getClientManager() {
        return this.clientManager;
    }
}
