package bot.network.server;

import bot.Bot;
import bot.logger.Logger;
import bot.messages.incoming.PacketMessage;
import bot.network.client.ClientManager;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.websocketx.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class WebSocketHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

            System.out.println("This is a WebSocket frame: " + msg);
            //System.out.println("Client Channel : " + ctx.channel());
            if (msg instanceof BinaryWebSocketFrame) {
                System.out.println("BinaryWebSocketFrame Received : ");
                System.out.println(((BinaryWebSocketFrame) msg).content());
            } else if (msg instanceof TextWebSocketFrame) {
                //System.out.println("TextWebSocketFrame Received : ");


                String message = ((TextWebSocketFrame) msg).text();

                System.out.println(message);

                try {
                    JSONObject jsonObject = new JSONObject(message);
                    int header = jsonObject.getInt("header");
                    JSONObject body = jsonObject.getJSONObject("body");

                    Bot.getLogger().log(Logger.PACKET, "[INCOMING] | header: " + header + " | body: " + body);
                    Bot.getWebSocketServer().getPacketManager().applyIncomingPacket(ctx.attr(ClientManager.CLIENT).get(), new PacketMessage(header, body));

                } catch(JSONException e) {
                    e.printStackTrace();
                }
            } else if (msg instanceof PingWebSocketFrame) {
                System.out.println("PingWebSocketFrame Received : ");
                System.out.println(((PingWebSocketFrame) msg).content());
            } else if (msg instanceof PongWebSocketFrame) {
                System.out.println("PongWebSocketFrame Received : ");
                System.out.println(((PongWebSocketFrame) msg).content());
            } else if (msg instanceof CloseWebSocketFrame) {
                System.out.println("CloseWebSocketFrame Received : ");
                System.out.println("ReasonText :" + ((CloseWebSocketFrame) msg).reasonText());
                System.out.println("StatusCode : " + ((CloseWebSocketFrame) msg).statusCode());
            } else {
                System.out.println("Unsupported WebSocketFrame");
            }
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext channelHandlerContext, Throwable cause) {
        if (cause instanceof Exception) {
            if (!(cause instanceof IOException))
                Bot.getLogger().log(Logger.ERROR, "[SERVER] exceptionCaught: " + cause + ", probably is not a valid request!");
        }

        channelHandlerContext.close();
    }
}
