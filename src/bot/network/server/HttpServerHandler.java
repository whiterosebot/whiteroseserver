package bot.network.server;

import bot.Bot;
import bot.logger.Logger;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.FullHttpMessage;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;


public class HttpServerHandler extends ChannelInboundHandlerAdapter  {
    WebSocketServerHandshaker handshaker;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        if (msg instanceof HttpRequest || msg instanceof FullHttpMessage) {

            HttpRequest httpRequest = (HttpRequest) msg;

            Bot.getLogger().log(Logger.DEBUG, "[BOT] Http Request Received");

            HttpHeaders headers = httpRequest.headers();
            Bot.getLogger().log(Logger.DEBUG, "[BOT]: " +headers.get("Connection"));
            Bot.getLogger().log(Logger.DEBUG, "[BOT]: Upgrade : " + headers.get("Upgrade"));

            if ("Upgrade".equalsIgnoreCase(headers.get(HttpHeaderNames.CONNECTION)) ||
                    "WebSocket".equalsIgnoreCase(headers.get(HttpHeaderNames.UPGRADE))) {

                //Adding new handler to the existing pipeline to handle WebSocket Messages
                ctx.pipeline().replace(this, "websocketHandler", new bot.network.server.WebSocketHandler());

                //System.out.println("WebSocketHandler added to the pipeline");
                Bot.getLogger().log(Logger.DEBUG, "Opened Channel : " + ctx.channel());
                System.out.println("Handshaking....");
                //Do the Handshake to upgrade connection from HTTP to WebSocket protocol
                handleHandshake(ctx, httpRequest);
                //System.out.println("Handshake is done");
            }
        } else {
            System.out.println("Incoming request is unknown");
        }
    }

    /* Do the handshaking for WebSocket request */
    protected void handleHandshake(ChannelHandlerContext ctx, HttpRequest req) {
        WebSocketServerHandshakerFactory wsFactory =
                new WebSocketServerHandshakerFactory(getWebSocketURL(req), null, true);
        handshaker = wsFactory.newHandshaker(req);
        if (handshaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            handshaker.handshake(ctx.channel(), req);
        }
    }

    @Override
    public void channelRegistered(ChannelHandlerContext channelHandlerContext) {
        if (!Bot.getWebSocketServer().getClientManager().addClient(channelHandlerContext))
            channelHandlerContext.close();
    }

    protected String getWebSocketURL(HttpRequest req) {
        System.out.println("Req URI : " + req.getUri());
        String url =  "ws://" + req.headers().get("Host") + req.getUri() ;
        System.out.println("Constructed URL : " + url);
        return url;
    }
}
